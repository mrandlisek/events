import express from 'express';
import {authorize} from '../service/Security.js'
import * as EventTypes from "../service/EventTypes.js";
import {deleteEventType} from "../service/EventTypes.js";

const router = express.Router();

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/", authorize('admin'), async function (req, res) {
    let eventTypes = await EventTypes.findAllEvents()
    res.render('event_types/index.twig', {eventTypes: eventTypes});
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/edit/:eventTypeId", authorize('admin'), async function (req, res) {
    let eventType = await EventTypes.findEventType(req.params.eventTypeId)
    res.render('event_types/edit.twig', {eventType: eventType[0]});
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.post("/edit/:eventTypeId", authorize('admin'), async function (req, res) {
    await EventTypes.updateEventType(req.body, req.params.eventTypeId)
    await req.flash('success', 'Typ udalosti bol úspešne aktualizovaný')
    res.redirect('/admin/event/types/')
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/add", authorize('admin'), function (req, res) {
    res.render('event_types/add.twig');
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.post("/add", authorize('admin'), async function (req, res) {
    await EventTypes.addEventType(req.body)
    await req.flash('success', 'Typ udalosti bol úspešne pridaný')
    res.redirect('/admin/event/types/')
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/remove/:eventTypeId", authorize('admin'), async function (req, res) {
    await EventTypes.deleteEventType(req.params.eventTypeId)
    await req.flash('success', 'Typ udalosti bol úspešne zmazaný')
    res.redirect('/admin/event/types/');
});

export {router as AdminEventTypesController}
