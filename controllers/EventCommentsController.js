import express from 'express';
import {query} from "../service/MariaClient.js";
import {authorize} from '../service/Security.js'
import * as Comments from "../service/Comments.js";

const router = express.Router();

/**
 * Vymazat prispevok a prejst na zoznam prispevkov
 */
router.get('/delete/:commentsId/:eventId', authorize('admin'), async function(req, res) {
    // pockat na dokoncenie funkcie pre pridanie prispevku
    await Comments.deleteComment(req.params.commentsId);
    await req.flash('success', 'Príspevok bol vymazaný.')

    // presmerovat na zobrazenie vsetkych prispevkov
    res.redirect('/detail/'+req.params.eventId);
})

/**
 * Pridat novy prispevok cez formular.
 *
 * Pridavat prispevky moze len prihlaseny pouzivatel s rolou user alebo admin.
 */
router.post("/new", authorize('user', 'admin'), async function (req, res) {
    // pockat na dokoncenie funkcie pre pridanie prispevku
    await Comments.addComment(req.body.eventId, req.body.message, req.body.name);
    await req.flash('success', 'Komentár bol pridaný.')

    // presmerovat na zobrazenie vsetkych prispevkov
    res.redirect('/detail/'+req.body.eventId);
});

export {router as EventCommentsController}
