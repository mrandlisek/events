import express from 'express';
import {authorize} from '../service/Security.js'
import * as Events from "../service/Events.js";
import * as Places from "../service/Places.js"
const router = express.Router();
import moment from 'moment';

/**
 * STRANKA s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/", authorize('admin'), async function (req, res) {
    let events = await Events.findAllEvents()
    res.render('events/index.twig',{events: events});
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/edit/:eventId", authorize('admin'), async function (req, res) {
    let event = await Events.findEvent(req.params.eventId)
    event = event[0]
    let regions = await Events.findAllRegions()
    let types = await Events.findAllTypes()
    let places = await Places.findAllPlaces()
    event.date = moment(event.date).format('YYYY-MM-DD HH:mm:ss')
    res.render('events/edit.twig', {event: event, regions: regions, types: types, places: places});
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.post("/edit/:eventId", authorize('admin'), async function (req, res) {
    await Events.editEvent(req.body, req.params.eventId)
    await req.flash('success', 'Udalosť bola úspešne aktualizovaná')
    res.redirect('/admin/events/')
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/add", authorize('admin'), async function (req, res) {
    let regions = await Events.findAllRegions()
    let types = await Events.findAllTypes()
    let places = await Places.findAllPlaces()
    res.render('events/add.twig', {regions: regions, types: types, places: places});
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.post("/add", authorize('admin'), async function (req, res) {
    await Events.addEvent(req.body, req.params.eventId)
    await req.flash('success', 'Udalosť bola úspešne pridaná')
    res.redirect('/admin/events/')
});

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/remove/:eventId", authorize('admin'), async function (req, res) {
    Events.deleteEvent(req.params.eventId)
    await req.flash('success', 'Udalosť bola úspešne zmazaná')
    res.redirect('/admin/events/')
});

export {router as AdminEventsController}
