import express from 'express';
import {authorize} from '../service/Security.js'
import * as Places from "../service/Places.js";
const router = express.Router();

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/", authorize('admin'), async function (req, res) {
    let places = await Places.findAllPlaces()
    res.render('places/index.twig',{places: places});
});

router.get('/edit/:placeId', authorize('admin'), async function(req, res){
    let place = await Places.findPlace(req.params.placeId)
    place = place[0]
    res.render('places/edit.twig', {place: place})
})

router.post('/edit/:placeId', authorize('admin'), async function(req, res){
    await Places.updatePlace(req.body, req.params.placeId)
    await req.flash('success', 'Miesto bolo aktualizované')
    res.redirect('/admin/places')
})

router.get('/add', authorize('admin'), async function(req, res){
    res.render('places/add.twig')
})

router.post('/add', authorize('admin'), async function(req, res){
    await Places.addPlace(req.body)
    await req.flash('success', 'Miesto bolo pridané')
    res.redirect('/admin/places')
})

/**
 * Stranka s obmedzenym pristupom. Vyzaduje sa rola admin.
 */
router.get("/remove/:placeId", authorize('admin'), async function (req, res) {
    await Places.deletePlace(req.params.placeId)
    await req.flash('success', 'Miesto bolo úspešne zmazané')
    res.redirect('/admin/places');
});

export {router as AdminPlacesController}
