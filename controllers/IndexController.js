import express from 'express';
import {authorize} from '../service/Security.js'
const router = express.Router();
import * as Events from '../service/Events.js'
import * as Comment from '../service/Comments.js'
import moment from "moment/moment.js";

/**
 * Uvodna stranka
 */
router.get("/", async function (req, res) {
    let events = await Events.findAllActualEvents()
    events.forEach(function(event){
        event.date = moment(event.date).format('HH:mm DD.MM.YYYY')
    })
    res.render('index/index.twig', {
        events: events
    });
});

/**
 * Detail podujatia
 */
router.get("/detail/:eventId", async function (req, res) {
    let event = await Events.detailEvent(req.params.eventId)
    let comments = await Comment.findAllComments(req.params.eventId)
    event = event[0]
    res.render('index/detail.twig', {
        event: event,
        comments: comments
    });
});

// /**
//  * Odoslanie dat z dormulara metodou post. Vyzaduje sa rola admin.
//  */
// router.post("/form", async function (req, res) {
//     console.log(req.body)
//     await req.flash('success', JSON.stringify(req.body));
//     res.redirect('/form');
// });

// router.get("/form",  function (req, res, next) {
//
//     res.render('index/form.twig');
// });

export {router as IndexController}


