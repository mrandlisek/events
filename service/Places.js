import * as Db from "./MariaClient.js";

/**
 * Vratit zoznam prispevkov vratane pouzivatelskeho mena autora zoradeny posla casu vytvorenia zostupne.
 * @returns {Promise<*>}
 */
async function findAllPlaces() {
    return Db.query('SELECT p.* FROM event_place p ORDER BY p.id DESC');
}

/**
 * @param placeId
 * @returns {Promise<void>}
 */
async function findPlace(placeId) {
    return Db.query('SELECT p.* FROM event_place p WHERE p.id = :placeId', {placeId: placeId})
}

/**
 * @param placeId
 * @returns {Promise<*>}
 */
async function deletePlace(placeId) {
    return Db.query('DELETE FROM event_place WHERE id = :placeId', {placeId: placeId});
}

async function updatePlace(data, placeId) {
    console.log(data)
    console.log(placeId)
    return Db.query('UPDATE event_place SET name= :name, street= :street, city= :city, number= :number, longitude= :longitude, latitude= :latitude WHERE id = :placeId', {
        placeId: placeId,
        name: data.name,
        street: data.street,
        city: data.city,
        number: data.number,
        longitude: data.longitude!== ''?data.longitude:null,
        latitude: data.latitude!== ''?data.longitude:null
    })
}

async function addPlace(data, placeId) {
    console.log(data)
    console.log(placeId)
    return Db.query('INSERT INTO event_place (name, street, city, number, longitude, latitude) VALUES (:name, :street, :city, :number, :longitude, :latitude)', {
        placeId: placeId,
        name: data.name,
        street: data.street,
        city: data.city,
        number: data.number,
        longitude: data.longitude!== ''?data.longitude:null,
        latitude: data.latitude!== ''?data.longitude:null
    })
}

export {findAllPlaces, deletePlace, findPlace, updatePlace, addPlace}
