import * as Db from "./MariaClient.js";

/**
 * Vratit zoznam prispevkov vratane pouzivatelskeho mena autora zoradeny posla casu vytvorenia zostupne.
 * @returns {Promise<*>}
 */
async function findAllEvents() {
    return Db.query('SELECT e.*, et.type FROM event e JOIN event_type et ON e.type = et.id ORDER BY e.date DESC');
}

async function findEvent(eventId) {
    return Db.query('SELECT e.* FROM event e WHERE e.id = :eventId', {eventId: eventId})
}

async function findAllRegions() {
    return Db.query('SELECT et.* FROM event_region et')
}

async function findAllTypes() {
    return Db.query('SELECT er.* FROM event_type er')
}

async function addEvent(data) {
    return Db.query('INSERT INTO event (name, description, type, date, place, region) VALUES (:name, :description, :type,:date, :place, :region)',
        {
            name: data.name,
            description: data.description,
            type: data.type,
            date: data.date,
            place: data.place,
            region: data.region
        }
    )
}

async function editEvent(data, id) {
    return Db.query('UPDATE event SET name= :name, description= :description, type= :type, date= :date, place= :place, region= :region  WHERE id = :id',
        {
        id: id,
        name: data.name,
        description: data.description,
        type: data.type,
        date: data.date,
        place: data.place,
        region: data.region
    })
}

async function deleteEvent(id){
    return Db.query('DELETE FROM event WHERE id = :id',{id: id})
}

async function findAllActualEvents(){
    return Db.query('SELECT e.name as eventName,e.id as eventId , e.date, et.*, er.*, ep.* FROM event e LEFT JOIN event_type et ON e.type = et.id LEFT JOIN event_place ep ON e.place = ep.id LEFT JOIN event_region er ON e.region = er.id WHERE date > NOW()')
}

async function detailEvent(eventId){
    return Db.query('SELECT e.id as eventId, e.*, et.*, er.*, ep.street as placeStreet, ep.number as placeNumber, ep.city as placeCity FROM event e LEFT JOIN event_type et ON e.type = et.id LEFT JOIN event_place ep ON e.place = ep.id LEFT JOIN event_region er ON e.region = er.id WHERE e.id = :id', {id: eventId})
}

export {findAllEvents, findEvent, findAllTypes, findAllRegions, addEvent, editEvent, deleteEvent, findAllActualEvents, detailEvent}
