import * as Db from "./MariaClient.js";

/**
 * Vymazat typ udalosti
 *
 * @param eventTypeId
 * @returns {Promise<*>}
 */
async function deleteEventType(eventTypeId) {
    await Db.query(
        'DELETE FROM event_type WHERE id = :eventTypeId',
        {eventTypeId: eventTypeId}
    );
}

/**
 * Vratit zoznam typov udalosti
 * @returns {Promise<*>}
 */
async function findAllEvents() {
    return Db.query('SELECT et.* FROM event_type et  ORDER BY et.id DESC');
}

/**
 * Vratit zoznam typov udalosti podla id
 * @returns {Promise<*>}
 */
async function findEventType(eventTypeId) {
    if (eventTypeId === undefined){
        return 'Typ podujatia neexistuje';
    }
    return Db.query('SELECT et.* FROM event_type et  WHERE et.id= ?', eventTypeId);
}

/**
 * @param data
 * @param eventTypeId
 * @returns {Promise<*>}
 */
async function updateEventType(data, eventTypeId){
    return Db.query('UPDATE event_type SET type = :type WHERE id = :eventTypeId' , {type: data.type, eventTypeId: eventTypeId})
}

async function addEventType(data){
    return Db.query('INSERT INTO event_type (type) VALUES (:type)', {type: data.type})
}

export {findAllEvents, deleteEventType, findEventType, updateEventType, addEventType}
