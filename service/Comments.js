import * as Db from "./MariaClient.js";

/**
 * Vlozit novy prispevok
 *
 * @param eventId
 * @param message
 * @returns {Promise<*>}
 */
async function addComment(eventId, message, name) {
    await Db.query(
        'INSERT INTO event_comment (event, date, message, name) VALUES (:eventId, now(), :message, :name)',
        {eventId: eventId, message: message, name: name}
    );
}

/**
 * Vymazat prispevok
 *
 * @param commentId
 * @returns {Promise<*>}
 */
async function deleteComment(commentId) {
    await Db.query(
        'DELETE FROM event_comment WHERE id = :commentId',
        {commentId: commentId}
    );
}

/**
 * Vratit zoznam prispevkov vratane pouzivatelskeho mena autora zoradeny posla casu vytvorenia zostupne.
 * @returns {Promise<*>}
 */
async function findAllComments(eventId) {
    return Db.query('SELECT c.* FROM event_comment c WHERE event = :eventId ORDER BY c.date DESC', {eventId: eventId});
}

export {addComment, findAllComments, deleteComment}
